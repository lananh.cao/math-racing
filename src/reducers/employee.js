import * as TYPES from "../constants/actions.types";

const INITIAL_STATE = {
  authenticated: false,
  loginFailed: false,
  isLoading: false,
  imageURI: "",
  data: {},
  errorAccount: ""
};

function EmployeeReducer(state = INITIAL_STATE, action) {
  console.log("Reducer called", state, action);
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        authenticated: true,
        loginFailed: false,
        data: action.payload
      };

    case "LOGIN_FAIL":
      return {
        ...state,
        authenticated: false,
        loginFailed: true,
        errorAccount: "Your username or password combination is incorrect"
      };

    case "RESET_STATE":
      return {
        ...state,
        authenticated: false,
        loginFailed: true
      };

    case "SHOW_LOADING":
      return {
        ...state,
        isLoading: true
      };

    case "HIDE_LOADING":
      return {
        ...state,
        isLoading: false
      };

    case TYPES.REGISTER:
      return {
        ...state,
        authenticated: true,
        loginFailed: false,
        data: action.payload
      };

    case "RESTORE_PASS":
      return {
        ...state,
        data: action.payload
      };

    case "UPDATE_AUTH":
      return {
        ...state,
        authenticated: action.value
      };

    case "LOGOUT":
      return {
        ...state,
        authenticated: false
      };

    case "SAVE_URI":
      console.log("called URI", action.payload);
      return {
        ...state,
        imageURI: action.payload
      };

    case "SAVE_URI":
      console.log("called URI", action.payload);
      return {
        ...state,
        imageURI: action.payload
      };

    default:
      return { ...state };
      break;
  }
}

export default EmployeeReducer;
