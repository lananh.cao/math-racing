import { combineReducers } from 'redux';
import EmployeeReducer from './employee';

const reducers = combineReducers({
  employeeReducer: EmployeeReducer
});

export default reducers;