import DeviceInfo from "react-native-device-info";
import * as TYPES from "../constants/actions.types";

export const login = (username, password) => {
  return (dispatch, getState) => {
    fetch("https://apiracing.starmathsonline.com.au/account/login", {
      headers: {
        accept: "*/*",
        "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7",
        "content-type": "application/json",
        "x-requested-with": "XMLHttpRequest"
      },
      body: JSON.stringify({
        UserName: username,
        Password: password,
        DeviceId: DeviceInfo.getDeviceId()
      }),
      method: "POST",
      mode: "cors"
    })
      .then(response => response.json())
      .then(responseJson => {
        setTimeout(() => {
          const state = getState().employeeReducer;
          if (responseJson.token !== "" && username !== "" && password !== "") {
            dispatch({
              type: "LOGIN",
              payload: {
                username,
                password
              }
            });
            dispatch({
              type: "HIDE_LOADING"
            });
          } else {
            dispatch({
              type: "LOGIN_FAIL"
            });
            dispatch({
              type: "HIDE_LOADING"
            });
          }
        }, 1000);
      });
  };
};

export const register = (first, last, email, password, type) => {
  return (dispatch, getState) => {
    fetch("https://apiracing.starmathsonline.com.au/account/Register", {
      headers: {
        accept: "*/*",
        "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7",
        "content-type": "application/json",
        "x-requested-with": "XMLHttpRequest"
      },
      body: JSON.stringify({
        FirstName: first,
        LastName: last,
        Email: email,
        Password: password,
        Type: type
      }),
      method: "POST",
      mode: "cors"
    })
      .then(response => response.json())
      .then(responseJson => {
        setTimeout(() => {
          const state = getState().employeeReducer;
          if (responseJson.token !== "") {
            dispatch({
              type: TYPES.REGISTER,
              payload: {
                username: email,
                password
              }
            });
          }
        }, 1000);
      });
  };
};

export const restorePass = email => {
  return (dispatch, getState) => {
    fetch("https://apiracing.starmathsonline.com.au/account/ForgotPassword", {
      headers: {
        accept: "*/*",
        "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7",
        "content-type": "application/json",
        "x-requested-with": "XMLHttpRequest"
      },
      body: JSON.stringify({
        Email: email
      }),
      method: "POST",
      mode: "cors"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log("retore pass", responseJson);
        setTimeout(() => {
          const state = getState().employeeReducer;
          if (responseJson.token !== "") {
            dispatch({
              type: "RESTORE_PASS",
              payload: responseJson.data
            });
          }
        }, 1000);
      });
  };
};

export const updateAuth = (value, nav = {}) => {
  console.log("action", value);
  return (dispatch, getState) => {
    dispatch({
      type: "UPDATE_AUTH",
      value: value == "true" ? true : false
    });

    if (value == "false") {
      nav.toLogin();
    } else nav.toHome();
  };
};

export const logout = () => {
  return (dispatch, getState) => {
    const state = getState().employeeReducer;
    dispatch({
      type: "LOGOUT"
    });
  };
};

export const resetState = () => {
  return (dispatch, getState) => {
    const state = getState().employeeReducer;
    dispatch({
      type: "RESET_STATE"
    });
  };
};

export const saveURI = uri => {
  return (dispatch, getState) => {
    dispatch({
      type: "SAVE_URI",
      payload: uri
    });
  };
};

const postImage = uri => {
  let formData = new FormData();
  formData.append("imageFile", {
    uri,
    type: "image/png",
    name: "name.png"
  });

  const apiUploadImageBody = {
    credentials: "include",
    headers: {
      accept: "*/*",
      "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7",
      "content-type": "multipart/form-data",
      "x-requested-with": "XMLHttpRequest"
    },
    referrer: "https://qam.starmathsonline.com.au/Feedback",
    referrerPolicy: "no-referrer-when-downgrade",
    body: formData,
    method: "POST",
    mode: "cors"
  };
  return fetch(
    "https://qam.starmathsonline.com.au/api/FeedbackApi/UploadImage",
    apiUploadImageBody
  );
};

export const uploadImage = (url, description) => {
  return (dispatch, getState) => {
    dispatch({
      type: "START_WAITING"
    });
    postImage(url)
      .then(response => response.json())
      .then(responseJson => {
        fetch("https://qam.starmathsonline.com.au/api/FeedbackApi/Create", {
          headers: {
            accept: "*/*",
            "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7",
            "content-type": "application/json",
            "x-requested-with": "XMLHttpRequest"
          },
          body: JSON.stringify({
            Mode: 0,
            ConceptCode: "",
            ScreenshotUrl: responseJson,
            Description: description,
            DeviceId: DeviceInfo.getDeviceId(),
            RefUserId: ""
          }),
          method: "POST",
          mode: "cors"
        })
          .then(response => response.json())
          .then(responseJson => {
            console.log("json", responseJson);
            dispatch({
              type: "UPLOAD_IMG_SUCCESS"
            });
            dispatch({
              type: "STOP_WAITING"
            });
          })
          .catch(err => {
            dispatch({
              type: "UPLOAD_IMG_FAILED"
            });
            dispatch({
              type: "STOP_WAITING"
            });
          });
      });
  };
};
