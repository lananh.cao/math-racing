import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#DCDCDC",
    height: "100%"
  },
  containerInside: {
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    width: "90%"
  },
  containerRow: {
    flexDirection: "row",
    alignItems: "center"
  },

  containerKeyboardAvoidingView: {
    flex: 1,
    backgroundColor: "#DCDCDC"
  },

  inputContainer: {
    borderColor: "#9BAAC9",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    borderWidth: 1,
    width: "90%",
    height: 45,
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  inputContainerDisable: {
    borderColor: "#9BAAC919",
    backgroundColor: "#9BAAC919",
    borderRadius: 5,
    borderWidth: 1,
    width: "90%",
    height: 45,
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  inputs: {
    height: 45,
    marginLeft: 10,
    borderBottomColor: "#FFFFFF",
    color: "#555E68",
    flex: 1
  },
  inputIcon: {
    marginLeft: 10,
    justifyContent: "center"
  },

  TextTitle: {
    fontSize: 20,
    color: "#fff"
  },
  TextContent: {
    fontSize: 14,
    color: "#616161"
  },
  TextAlignCenter: {
    textAlign: "center"
  },
  TextInducationRight: {
    paddingLeft: "auto",
    paddingRight: 5,
    color: "#9BAAC9",
    fontSize: 12
  },

  Button: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
    width: "80%",
    borderRadius: 5,
    backgroundColor: "#2B7EDA"
  },
  ButtonTextColor: {
    color: "white"
  },
  ButtonText: {
    textDecorationLine: "underline"
  },
  ButtonBound: {
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 5,
    color: "white",
    lineHeight: 30,
    marginLeft: 3,
    marginRight: 3,
    borderWidth: 1,
    borderColor: "#1665DB"
  },
  line: {
    width: "100%",
    borderWidth: 1,
    borderColor: "#DBE0EC"
  },
  buttonActive: {
    backgroundColor: "#fff",
    color: "#2b7eda",
    borderColor: "#1FB0E4",
    minWidth: 75,
    maxWidth: 105,
    textAlign: "center",
    paddingBottom: 3,
    paddingTop: 3,
    overflow: "hidden"
  },
  buttonNormal: {
    color: "#fff",
    borderColor: "#1FB0E4",
    minWidth: 75,
    maxWidth: 105,
    textAlign: "center",
    paddingBottom: 3,
    paddingTop: 3
  }
});

export default styles;
