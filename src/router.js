import React from "react";
import { Scene, Router, Actions, Stack } from "react-native-router-flux";

import Root from "./components/root";
import Home from "./components/home";
import Login from "./components/login-screen";
import Register from "./components/register-screen";
import ForgotPassword from "./components/forgot-password-screen";

const MyRouter = () => (
  <Router>
    <Stack key="root" hideNavBar={true}>
      <Scene key="root_home" component={Root} />
      <Scene key="home" component={Home} />
      <Scene key="login" component={Login} />
      <Scene key="register" component={Register} />
      <Scene key="restore_password" component={ForgotPassword} />
    </Stack>
  </Router>
);
export default MyRouter;
