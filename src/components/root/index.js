import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as EmployeeActions from "../../actions";

class Root extends Component {
  componentDidMount() {
    this.getData();
    console.log("current", Actions.currentScene);
  }

  componentDidUpdate() {
    const { authenticated: temp } = this.props
    console.log('this.props.authenticated', temp);
  }

  async getData() {
    try {
      let value = await AsyncStorage.getItem("@auth")
      if (value != null) {  
        await AsyncStorage.setItem("@auth", value);
        const nav = {
          toHome: () => Actions.home(),
          toLogin: () => Actions.login()
        }
        this.props.eAction.updateAuth(value, nav);
      } else {       
        value = "false";
        await AsyncStorage.setItem("@auth", "false");
        const nav = {
          toHome: () => Actions.home(),
          toLogin: () => Actions.login()
        }
        this.props.eAction.updateAuth(value, nav); 
      }
    } catch (error) {}
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.employeeReducer.authenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    eAction: bindActionCreators(EmployeeActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Root)
