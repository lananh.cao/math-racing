import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text,
  CheckBox,
  ImageBackground,
  ActivityIndicator,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AsyncStorage from "@react-native-community/async-storage";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Orientation from "react-native-orientation";

import * as EmployeeActions from "../../actions";
import styles from "../../styles";
import Header from "../header";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FirstName: "",
      LastName: "",
      Email: "",
      Password: "",
      errorFirstName: false,
      errorLasstName: false,
      errorEmail: false,
      errorPassword: false,
      flag: false,
      isRegistering: false
    };
  }
  componentDidMount() {
    Orientation.lockToPortrait();
  }

  checkInput(key, value) {
    if (key == "FirstName") {
      this.setState({ FirstName: value, errorFirstName: value == "" });
    } else if (key == "LastName") {
      this.setState({ LastName: value, errorLasstName: value == "" });
    } else if (key == "Email") {
      this.setState({ Email: value });
      if (value && value.indexOf("@") != -1 && value.indexOf(".") != -1) {
        this.setState({ errorEmail: false });
      } else {
        this.setState({ errorEmail: true });
      }
    } else if (key == "Password") {
      this.setState({ Password: value, errorPassword: value == "" });
    }
  }

  showLoader = () => {
    console.log("isloading");
    this.setState({ isRegistering: true });
  };

  handlNavigationEvent = () => {
    if (
      this.state.FirstName == "" ||
      this.state.LastName == "" ||
      this.state.Email == "" ||
      this.state.Password == ""
    ) {
      AsyncStorage.setItem("@auth", "false");
      const nav = {
        toHome: () => Actions.reset("home"),
        toLogin: () => Actions.reset("register")
      };
      this.props.eAction.updateAuth("false", nav);
    } else if (this.state.Email !== "" || this.state.Password !== "") {
      this.props.eAction.register(
        this.state.FirstName,
        this.state.LastName,
        this.state.Email,
        this.state.Password,
        this.state.flag ? 1 : 0
      );
      AsyncStorage.setItem("@auth", "true");
      const nav = {
        toHome: () => Actions.reset("home"),
        toLogin: () => Actions.reset("register")
      };
      this.props.eAction.updateAuth("true", nav);
    }
  };

  renderToggles() {
    return (
      <View style={{ flexDirection: "row", paddingHorizontal: "5%" }}>
        <View
          style={{
            flex: 4,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.setState({ flag: !this.state.flag });
            }}
          >
            <MaterialCommunityIcons
              name={
                this.state.flag == true
                  ? "checkbox-blank-circle-outline"
                  : "checkbox-marked-circle-outline"
              }
              style={{ fontSize: 25 }}
            />
          </TouchableOpacity>
          <Text>Parent</Text>
        </View>
        <View style={{ flex: 1 }} />
        <View
          style={{
            flex: 4,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.setState({ flag: !this.state.flag });
            }}
          >
            <MaterialCommunityIcons
              name={
                this.state.flag == false
                  ? "checkbox-blank-circle-outline"
                  : "checkbox-marked-circle-outline"
              }
              style={{ fontSize: 25 }}
            />
          </TouchableOpacity>
          <Text>Teacher</Text>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../assets/splash.png")}
          style={[styles.container, { justifyContent: "center" }]}
        >
          <View style={[styles.containerInside]}>
            <Header />
            <View
              style={{
                flexDirection: "row",
                paddingHorizontal: "5%",
                justtifyContent: "space-between"
              }}
            >
              <View
                style={[
                  styles.inputContainer,
                  this.state.errorFirstName ? styleLocal.errorBox : null,
                  { flex: 4.75 }
                ]}
              >
                <TextInput
                  style={styles.inputs}
                  placeholder="First name"
                  underlineColorAndroid="transparent"
                  onChangeText={FirstName =>
                    this.checkInput("FirstName", FirstName)
                  }
                />
              </View>
              <View style={{ flex: 0.5 }} />
              <View
                style={[
                  styles.inputContainer,
                  this.state.errorLasstName ? styleLocal.errorBox : null,
                  { flex: 4.75 }
                ]}
              >
                <TextInput
                  style={styles.inputs}
                  placeholder="Last name"
                  underlineColorAndroid="transparent"
                  onChangeText={LastName =>
                    this.checkInput("LastName", LastName)
                  }
                />
              </View>
            </View>
            <View
              style={[
                styles.inputContainer,
                this.state.errorEmail ? styleLocal.errorBox : null
              ]}
            >
              <TextInput
                style={styles.inputs}
                placeholder="Email address"
                underlineColorAndroid="transparent"
                keyboardType="email-address"
                onChangeText={Email => this.checkInput("Email", Email)}
              />
            </View>

            <View
              style={[
                styles.inputContainer,
                this.state.errorPassword ? styleLocal.errorBox : null
              ]}
            >
              <TextInput
                style={styles.inputs}
                placeholder="Your password"
                underlineColorAndroid="transparent"
                secureTextEntry={false}
                onChangeText={Password => this.checkInput("Password", Password)}
              />
            </View>

            {this.renderToggles()}

            <View
              style={[
                styles.containerRow,
                { paddingBottom: 20, paddingTop: 20 }
              ]}
            >
              <Text style={[styles.ButtonText, { textDecorationLine: "none" }]}>
                Already have an account.
              </Text>
              <TouchableOpacity onPress={() => Actions.reset("login")}>
                <Text
                  style={[
                    styles.ButtonText,
                    { color: "#1691ED", paddingLeft: 5 }
                  ]}
                >
                  Login now!
                </Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={[styles.Button, { marginBottom: 30 }]}
              onPress={() => {
                this.handlNavigationEvent();
                // this.props.eAction.register(
                //   this.state.FirstName,
                //   this.state.LastName,
                //   this.state.Email,
                //   this.state.Password,
                //   this.state.flag ? 1 : 0
                // );
                // AsyncStorage.setItem("@auth", "true");
                // const nav = {
                //   toHome: () => Actions.reset("home"),
                //   toLogin: () => Actions.login()
                // };
                // this.props.eAction.updateAuth("true", nav);
                //this.showLoader();
              }}
            >
              {this.state.isRegistering ? (
                <ActivityIndicator size="large" color="#0000ff" />
              ) : (
                <Text style={styles.ButtonTextColor}>Register</Text>
              )}
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    employeeReducer: state.employeeReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    eAction: bindActionCreators(EmployeeActions, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);

const styleLocal = StyleSheet.create({
  errorBox: {
    borderColor: "red",
    borderWidth: 1
  }
});
