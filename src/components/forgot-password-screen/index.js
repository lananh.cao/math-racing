import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  Image,
  ActivityIndicator
} from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as EmployeeActions from "../../actions";
import styles from "../../styles";
import Header from "../header";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Email: "",
      errorEmail: false,
      isRequestSending: false
    };
  }

  checkInput(value) {
    this.setState({ Email: value });
    if (value && value.indexOf("@") != -1 && value.indexOf(".") != -1) {
      this.setState({ errorEmail: false });
    } else {
      this.setState({ errorEmail: true });
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../assets/splash.png")}
          style={[styles.container, { justifyContent: "center" }]}
        >
          <View
            style={{
              position: "absolute",
              top: 0,
              left: -10,
              width: "100%",
              padding: 10
            }}
          >
            <TouchableOpacity
              style={{ marginLeft: 20, marginTop: 10 }}
              onPress={() => Actions.pop()}
            >
              <Image source={require("../../../assets/ic_arrow_back.png")} />
            </TouchableOpacity>
          </View>

          <View style={[styles.containerInside]}>
            <Header />
            <Text style={[styles.TextContent, { width: "80%" }]}>
              Enter your registered email, and we'll send you an email to reset
              your account.
            </Text>
            <View style={{ paddingBottom: 20 }} />
            <View
              style={[
                styles.inputContainer,
                { marginBottom: 100 },
                this.state.errorEmail ? styleLocal.errorBox : null
              ]}
            >
              <Image
                style={styles.inputIcon}
                source={require("../../../assets/ic_email.png")}
              />
              <TextInput
                style={styles.inputs}
                placeholder="Email"
                underlineColorAndroid="transparent"
                onChangeText={Email => this.checkInput(Email)}
              />
            </View>
            <TouchableOpacity
              style={[styles.Button, { marginBottom: 30 }]}
              onPress={() => {
                this.props.eAction.restorePass(this.state.Email);
                alert("Check your email to reset password");
              }}
            >
              {this.state.isRequestSending ? (
                <ActivityIndicator size="large" color="#0000ff" />
              ) : (
                <Text style={styles.ButtonTextColor}>RESET PASSWORD</Text>
              )}
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    employeeReducer: state.employeeReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    eAction: bindActionCreators(EmployeeActions, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);

const styleLocal = StyleSheet.create({
  errorBox: {
    borderColor: "red",
    borderWidth: 1
  }
});
