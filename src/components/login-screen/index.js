import React, { Component } from "react";
import {
  ActivityIndicator,
  Text,
  View,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import Header from "../header.js";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AsyncStorage from "@react-native-community/async-storage";
import Orientation from "react-native-orientation";

import * as EmployeeActions from "../../actions";
import styles from "../../styles";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isLoginning: false,
      emptyEmail: false,
      emptyPassword: false
    };
  }

  componentDidMount() {
    Orientation.lockToPortrait();
  }

  onClickListener = viewId => {
    console.log("onClickListener");
    if (viewId == "login") {
      if (this.state.isLoginning) {
        return;
      }

      if (this.state.email == "") {
        this.setState({
          emptyEmail: true
        });
      } else if (this.state.password == "") {
        this.setState({
          emptyPassword: true
        });
      } else {
        this.setState({ isLoginning: true });
      }
    } else if (viewId == "restore_password") {
      Actions.restore_password();
    } else if (viewId == "register") {
      Actions.register();
    }
    console.log("onClickListener", this.state.emptyPassword);
  };

  handlNavigationEvent = () => {
    if (this.state.email == "" || this.state.password == "") {
      console.log("login failed");
      console.log("emptyEmail", this.state.email);
      AsyncStorage.setItem("@auth", "false");
      const nav = {
        toHome: () => Actions.reset("home"),
        toLogin: () => Actions.login()
      };
      this.props.eAction.updateAuth("false", nav);
    } else if (this.state.email !== "" || this.state.password !== "") {
      this.props.eAction.login(this.state.email, this.state.password);
      AsyncStorage.setItem("@auth", "true");
      const nav = {
        toHome: () => Actions.reset("home"),
        toLogin: () => Actions.login()
      };
      this.props.eAction.updateAuth("true", nav);
    }
  };

  TextIsChanging = (inputName, value) => {
    if (this.props.employeeReducer.errorAccount != "") {
      this.setState({
        emptyEmail: false,
        emptyPassword: false
      });
    }

    if (inputName == "email") {
      if (value == "") {
        this.setState({
          emptyEmail: true,
          email: value
        });
      } else {
        this.setState({
          emptyEmail: false,
          email: value
        });
      }
    } else if (inputName == "password") {
      if (value == "") {
        this.setState({
          emptyPassword: true,
          password: value
        });
      } else {
        this.setState({
          emptyPassword: false,
          password: value
        });
      }
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../assets/splash.png")}
          style={[styles.container, { justifyContent: "center" }]}
        >
          <View style={[styles.containerInside, { width: "92%" }]}>
            <Header />
            <TouchableWithoutFeedback
              style={{ flex: 1 }}
              onPress={() => Keyboard.dismiss()}
            >
              <KeyboardAvoidingView
                style={[styles.containerInside]}
                behavior="padding"
                enabled
              >
                <Text
                  style={{
                    color: "#cc3300",
                    height: 25,
                    marginTop: -25,
                    marginLeft: -24,
                    marginRight: -25
                  }}
                >
                  {this.props.employeeReducer.errorAccount}
                </Text>
                <View
                  style={[
                    styles.inputContainer,
                    {
                      borderColor: this.state.emptyEmail ? "#cc3300" : "#9BAAC9"
                    }
                  ]}
                >
                  <Image
                    style={styles.inputIcon}
                    source={require("../../../assets/ic_email.png")}
                  />
                  <TextInput
                    style={styles.inputs}
                    placeholder={
                      this.state.emptyEmail == true
                        ? "Please enter your username"
                        : "Email or Username"
                    }
                    placeholderTextColor={
                      this.state.emptyEmail ? "#cc3300" : null
                    }
                    value={this.state.email}
                    keyboardType="email-address"
                    underlineColorAndroid="transparent"
                    onChangeText={email => this.TextIsChanging("email", email)}
                  />
                </View>

                <View
                  style={[
                    styles.inputContainer,
                    {
                      borderColor: this.state.emptyPassword
                        ? "#cc3300"
                        : "#9BAAC9"
                    }
                  ]}
                >
                  <Image
                    style={styles.inputIcon}
                    source={require("../../../assets/ic_password.png")}
                  />
                  <TextInput
                    style={styles.inputs}
                    placeholder={
                      this.state.emptyPassword == true
                        ? "Please enter your password"
                        : "Password"
                    }
                    placeholderTextColor={
                      this.state.emptyPassword ? "#cc3300" : null
                    }
                    value={this.state.password}
                    secureTextEntry={true}
                    underlineColorAndroid="transparent"
                    onChangeText={password =>
                      this.TextIsChanging("password", password)
                    }
                  />
                </View>

                <TouchableOpacity
                  onPress={() => this.onClickListener("restore_password")}
                >
                  <Text
                    style={[
                      styles.ButtonText,
                      { paddingBottom: 30, paddingTop: 20, color: "#9BAAC9" }
                    ]}
                  >
                    Forgot password?
                  </Text>
                </TouchableOpacity>

                <View style={styles.containerRow}>
                  <Text
                    style={[styles.ButtonText, { textDecorationLine: "none" }]}
                  >
                    Not registered?
                  </Text>
                  <TouchableOpacity
                    onPress={() => this.onClickListener("register")}
                  >
                    <Text
                      style={[
                        styles.ButtonText,
                        { color: "#1691ED", paddingLeft: 5 }
                      ]}
                    >
                      Join now!
                    </Text>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  style={[styles.Button, { marginBottom: 30, marginTop: 30 }]}
                  onPress={() => {
                    this.onClickListener("login");
                    this.handlNavigationEvent();
                  }}
                >
                  {/* {this.state.isLoginning ? (
                    <ActivityIndicator size="large" color="#0000ff" />
                  ) : (
                    <Text style={styles.ButtonTextColor}>LOGIN</Text>
                  )} */}
                  <Text style={styles.ButtonTextColor}>LOGIN</Text>
                </TouchableOpacity>
              </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    employeeReducer: state.employeeReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    eAction: bindActionCreators(EmployeeActions, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
