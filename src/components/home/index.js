import React, { Component } from "react";
import {
  WebView,
  View,
  PermissionsAndroid,
  CameraRoll,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import ActionButton from "react-native-action-button";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import DeviceInfo from "react-native-device-info";
import Orientation from "react-native-orientation";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as EmployeeActions from "../../actions";
import { captureScreen } from "react-native-view-shot";


class Home extends Component {
  constructor(props) {
    super(props);
    this.uri = "";
    this.state = {
      username: "",
      password: "",
      description: "",
      url: "",
      flag: false,
      showPopup: false
    };
    this.getInfo();
  }

  takeScreenShot = () => {
    captureScreen({
      format: "png",
      quality: 0.8
    })
      .then(uri => {
        const debug = true;
        if (debug) {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: "Cool Photo App Camera Permission",
              message:
                "Cool Photo App needs access to your camera " +
                "so you can take awesome pictures.",
              buttonNeutral: "Ask Me Later",
              buttonNegative: "Cancel",
              buttonPositive: "OK"
            }
          ).then(granted => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) { 
              this.props.eAction.saveURI(uri)
              this.uri = uri;
              // Your Save flow
              // CameraRoll.saveToCameraRoll(uri).then(() => {
              //   console.log("saved");
              // });
              this.setState({ showPopup: true })
            }
          });
        } else {
          this.setState({ imageURI: uri });
          this.uploadImage(uri);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };


  getData = async () => {
    try {
      const value = await AsyncStorage.getItem("@auth")
      if (value !== null) {
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  getInfo = async () => {
    try {
      let username = await AsyncStorage.getItem("@username")
      let password = await AsyncStorage.getItem("@password")
      console.log(username, password)
      let url = `http://racing.starmathsonline.com.au/?username=${username}&password=${password}`
      this.setState({url: url})
    } catch (e) {
      console.log("error", e);
    }
  };

  storeData = async () => {
    try {
      await AsyncStorage.setItem("@auth", "false");
      await AsyncStorage.setItem("@username", "")
      await AsyncStorage.setItem("@password", "")
      this.props.eAction.logout();

    } catch (e) {
      console.log("error", e);
    }
  };

  storeInfo = async () => {
    try {
      console.log("storeinfo")
      const { username, password } = this.props.employeeReducer;
      await AsyncStorage.setItem("@username", this.props.employeeReducer.data.username)
      await AsyncStorage.setItem("@password", this.props.employeeReducer.data.password)
      if (this.state.flag == false) {
        let url = `http://racing.starmathsonline.com.au/?username=${this.props.employeeReducer.data.username}&password=${this.props.employeeReducer.data.password}`
        this.setState({url: url, flag: true});
      }     
    } catch (e) {
      console.log("error", e);
    }
  };

  componentDidMount() {
    Orientation.lockToLandscape();
    this.getData();
    console.log("reducer Didmount", this.props.employeeReducer)
  }

  componentDidUpdate() {
    const { authenticated } = this.props.employeeReducer;
    if (authenticated == false) {
      Actions.reset("login");
    }
    console.log("reducer Didupdate", this.props.employeeReducer)
    const { username, password } = this.props.employeeReducer.data
    if (username != null && password != null) {
      this.storeInfo();
    }
    console.log("Home", this.props)
  }

  renderButton() {
    return (
      <ActionButton
        buttonColor="rgba(231,76,60,1)"
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          zIndex: 5
        }}
      >
        <ActionButton.Item
          buttonColor="#9b59b6"
          title="Feedback"
          onPress={() => {
            this.takeScreenShot();
          }}
        >
          <MaterialCommunityIcons
            name="cellphone-screenshot"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#3498db"
          title="Logout"
          onPress={() => {
            this.storeData();  
          }}
        >
          <MaterialCommunityIcons
            name="logout-variant"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
      </ActionButton>
    );
  }

  renderPopup() {
    if (this.state.showPopup) {
      return (
        <View
          style={{
            width: 500, height: 300,
            position: 'absolute',
            top: 50,
            left: 150,
            backgroundColor: 'white',
            zIndex: 3,
            borderRadius: 10
          }}
        >
          <View style={{flex: 1.5, borderBottomWidth: 2, borderBottomColor: "#ecf0f1", justifyContent: "center", alignItems: "center"}}>
            <Text style={{color: "#34495e", fontWeight: "bold"}}>FEEDBACK</Text>
          </View>
          <View style={{flex: 6.5}}>
            <TextInput
              placeholder={ "Write your description" }
              value={this.state.description}
              underlineColorAndroid="transparent"
              onChangeText={description => this.setState({description: description})}
            />
          </View>
          <View style={{flex: 2, flexDirection: "row"}}>
            <TouchableOpacity 
              style={{flex: 1, backgroundColor: "#bdc3c7", flexDirection: "row", justifyContent: "center", alignItems: "center"}}
              onPress={() => this.setState({showPopup: false})}
            >
              <MaterialCommunityIcons
                name="arrow-left"
                style={{color: "white", fontSize: 20, padding: 30, left: -40 }}
              />
              <TouchableOpacity title="Cancel">
                <Text style={{color: "white", fontSize: 15, left: -30 }}>Cancel</Text>
              </TouchableOpacity>
            </TouchableOpacity>
          
            <TouchableOpacity
              style={{flex: 1, backgroundColor: "#e74c3c", flexDirection: "row", justifyContent: "center", alignItems: "center"}}
              onPress={() => {
                  this.props.eAction.uploadImage(this.uri, this.state.description);
                  this.setState({showPopup: false});
                }}
            >
              <TouchableOpacity title="Send">
                <Text style={{color: "white", fontSize: 15, left: 20 }}>Send</Text>
              </TouchableOpacity>
              <MaterialCommunityIcons
                name="arrow-right"
                style={{color: "white", fontSize: 20, padding: 30, left: 30}}
              />
            </TouchableOpacity>   
          </View>
        </View>
      )
    }

    return null
  }

  render() {
    return (
      <View style={{ flex: 1 }} collapsable={false}>
        <WebView
          domStorageEnabled={true}
          javaScriptEnabled={true}
          startInLoadingState={true}
          source={{ uri: this.state.url }}
          style={{ zIndex: 1, flex: 1 }}
          scalesPageToFit={true}
          ignoreSslError={true}
        />
        {this.renderButton()}
        {this.renderPopup() }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    employeeReducer: state.employeeReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    eAction: bindActionCreators(EmployeeActions, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});
