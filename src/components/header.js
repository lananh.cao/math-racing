import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../assets/logo.png')} />
        <Text style={{color: '#00B7FF', fontSize: 25, marginTop: 2, fontWeight:'bold'}}>RACING</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  }
});
